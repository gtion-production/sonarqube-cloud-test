﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class GameController : MonoBehaviour {

	[SerializeField]AudioClip[] soundAnimal = null;

	float[] place = new float[]{0 , -15.9f , -31.8f , -47.7f};

	int idx = 0;

	void Start () {
		idx = 0;
		MoveToNextPosition (idx);
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			if (idx > 0) {
				idx--;
				MoveToNextPosition (idx);
				Debug.Log (place [idx]);
			}
		} else if (Input.GetKeyDown (KeyCode.RightArrow)) {
			if (idx < place.Length-1) {
				idx++;
				MoveToNextPosition (idx);
				Debug.Log (place [idx]);
			}
		
		}
		
	}

	/// <summary>
	/// Moves memindahkan ke posisi selanjutnya
	/// and Play Music
	/// </summary>
	/// <param name="indexposition">index dari array</param>
	void MoveToNextPosition(int indexposition){
		this.transform.position = Vector3.MoveTowards (this.transform.position, new Vector3 (place [indexposition], 0 , 0), 20f);
		GetComponent<AudioSource> ().clip = soundAnimal [indexposition];
		GetComponent<AudioSource> ().Play ();

	}
}
