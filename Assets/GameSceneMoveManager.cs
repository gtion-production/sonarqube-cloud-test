﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneMoveManager : MonoBehaviour {

	[SerializeField]string nextScene = null;
	[SerializeField]string previousScene = null;
	[SerializeField]bool isQuitNext = false;
	[SerializeField]GameObject Background = null;

	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.Escape)){
			if (!isQuitNext && previousScene != null)
				StartCoroutine (LoadingOperation (previousScene));
			else
				Application.Quit();
		}else if(Input.GetKeyDown(KeyCode.Return)){
			if (nextScene != null)
			StartCoroutine (LoadingOperation (nextScene));
		}
		
	}


	IEnumerator LoadingOperation(string sceneName){
		AsyncOperation loadingScene = SceneManager.LoadSceneAsync (sceneName);

		while(loadingScene.isDone == false){
			Debug.Log (loadingScene.progress);

			yield return false;
		}

	}
		
}
